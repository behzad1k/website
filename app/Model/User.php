<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP User
 */
class User extends AppModel {
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id'
        )
    );
//    public $validate = array(
//        'username' => array(
//            'alphaNumeric' => array(
//                'rule' => 'alphaNumeric',
//                'required' => false,
//                'message' => 'Letters and numbers only'
//            ),
//            'between' => array(
//                'rule' => array('lengthBetween', 5, 15),
//                'message' => 'Between 5 to 15 characters'
//            )),
//        'password' => array(
//            'rule' => array('minLength', 8),
//            'message' => 'Minimum 8 characters long'
//        ),
//        'email' => 'email',
//        'phone' => array(
//            'rule' => '/^[0][9]\w{9}$/i',
//            'message' => 'invalid phone number'
//        ),
//        'birthdate' => array(
//            'date' => array(
//                'rule' => 'date',
//                'required' => false,
//                'allowEmpty' => true,
//                'message' => 'Letters and numbers only'
//            )),
//    );
    public function beforeSave($option = array()) {
        $passHash = new SimplePasswordHasher();
        if(isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = $passHash->hash(
                @$this->data[$this->alias]['password']
            );

        }
    }
}
