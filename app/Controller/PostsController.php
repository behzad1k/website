<?php

App::uses('AppController', 'Controller');

/**
 * CakePHP PostsController
 */
class PostsController extends AppController {

    public function index() {
        $this->pageOptions = array('pageTitle' => 'فهرست پست ها',
            'breadCrumbs' => array('پست ها', 'فهرست'));
        $this->set('pageOptions',$this->pageOptions);
        $this->panel();
        $posts = $this->Post->find("all");
        $this->set('posts',$posts);
    }
    public function add(){
        $this->pageOptions = array('pageTitle' => 'افزودن پست ',
            'breadCrumbs' => array('پست ها', 'افزودن'));
        $this->set('pageOptions',$this->pageOptions);
        if($this->request->is('post')){
            $this->Post->create();
            if($this->Post->save($this->request->data)){
                $postInsertId = $this->Post->getInsertID();
                $this->systemLogger('0201', $postInsertId);
                $this->Session->setFlash('record added', 'default', array(
                    'class' => 'alert alert-success'
                ));
                if($this->data['Post']['pic']){
                	print_r($this->data['Post']);
                	exit;
                    $this->RayaUpload->file = $this->data['Post']['pic'];
                    $this->RayaUpload->fileName = basename($this->RayaUpload->file['name']);
                    $this->RayaUpload->allowExtention = array('jpeg','jpg','png');
                    $this->RayaUpload->pathName = WWW_ROOT.DS.'uploads/posts'.DS.$this->RayaUpload->fileName;
                    $upload = $this->RayaUpload->upload();
                    if($upload == 1){
                        $this->Post->id = $postInsertId;
                        $this->Post->saveField('picture',$this->RayaUpload->fileName);
                    }
				}

                $this->redirect(array(
                    "controller" => "posts",
                    "action" => "add"
                ));
            }
            else {
                $this->Session->setFlash('record not added', 'default', array(
                    'class' => 'alert alert-important'
                ));
            }
        }
        $categories = $this->Category->find('all');
        $catOptions = array();
        foreach ($categories as $cat){
            $catOptions[$cat['Category']['id']] = $cat['Category']['title'];
        }
        $this->set('catOptions',$catOptions);
        array_push($this->options['scripts'],'/theme/Adminlab/assets/ckeditor/ckeditor.js','jquery.blockui.js','excanvas.js','respond.js','/theme/Adminlab/assets/fancybox/source/jquery.fancybox.pack.js');
        $this->panel();
    }
    public function delete($id = null){
        $posts = $this->Post->findById($id);
        if($posts){
            $this->Post->id = $id;
            $this->Post->saveField('del','0');
            $this->systemLogger('0103', $id);
            $this->Session->setFlash('record deleted', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            $this->redirect(array(
                "controller" => "posts",
                "action" => "index"
            ));
        }
        else{
            $this->Session->setFlash('record not found', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            $this->redirect(array(
                "controller" => "post",
                "action" => "index"
            ));
        }
    }
    public function edit($id = null){
        $this->pageOptions = array('pageTitle' => 'ویرایش پست ',
            'breadCrumbs' => array('پست ها', 'ویرایش'));
        $this->set('pageOptions',$this->pageOptions);
        $this->panel();
        if($this->request->is('post')){
            $this->Post->id = $id;
            if($this->Post->save($this->request->data)){
                $this->systemLogger('0202', $id);
                $this->Session->setFlash('record updated', 'default', array(
                        'class' => 'alert alert-success'
                    ));
                $this->redirect(array(
                    "controller" => "posts",
                    "action" => "index"
                ));

            }
            else {
                 $this->Session->setFlash('record not updated', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            }
        }
        $posts = $this->Post->find("all",array(
            'conditions' => array(
                'Post.id' => $id
            )
        ));
//        $datetime = explode(' ',$posts[0]['Post']['created']);
//        $date = explode('-',$datetime[0]);
//        $jdate = $this->JDate->gregorian_to_jalali($date[0], $date[1], $date[2]);
        if(!$posts){
            $this->Session->setFlash('record not found', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            $this->redirect(array(
                "action" => 'index'
            ));
        }
        $this->set('post',$posts[0]);
        $categories = $this->Category->find('all');
        $catOptions = array();
        foreach ($categories as $cat){
            $catOptions[$cat['Category']['id']] = $cat['Category']['title'];
        }
        $this->set('catOptions',$catOptions);
    }
    public function indexCategory(){
        $this->pageOptions = array('pageTitle' => 'دسته بندی ها',
            'breadCrumbs' => array('دسته بندی ها', 'فهرست'));
        $this->set('pageOptions',$this->pageOptions);
        $categories = $this->Category->find("all");
        $this->set('categories',$categories);
		$this->panel();

    }
    public function addCategory(){
		$this->pageOptions = array('pageTitle' => 'افزودن دسته بندی',
			'breadCrumbs' => array('دسته بندی ها', 'افزودن'));
		$this->set('pageOptions',$this->pageOptions);
		if($this->request->is('post')){
            $this->Category->create();
            if($this->Category->save($this->request->data)){
                    $this->systemLogger('0301', $this->Category->getInsertID());
                $this->Session->setFlash('record added', 'default', array(
                    'class' => 'alert alert-success'
                ));
                $this->redirect(array(
                    "controller" => "posts",
                    "action" => "addCategory"
                ));
            }
            else {
                $this->Session->setFlash('record not added', 'default', array(
                    'class' => 'alert alert-important'
                ));
            }
        }
        $this->panel();
    }
    public function editCategory($id = null){
        $this->pageOptions = array('pageTitle' => 'ویرایش دسته بندی ها',
            'breadCrumbs' => array('دسته بندی ها', 'ویرایش'));
        $this->set('pageOptions',$this->pageOptions);
        $this->panel();
        if($this->request->is('post')){
            $this->Category->id = $id;
            if($this->Category->save($this->request->data)){
                $this->systemLogger('0302', $id);
                $this->Session->setFlash('record updated', 'default', array(
                        'class' => 'alert alert-success'
                    ));
                $this->redirect(array(
                    "controller" => "posts",
                    "action" => "indexCategory"
                ));
            }
            else {
                 $this->Session->setFlash('record not updated', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            }
        }
        $categories = $this->Category->find("all",array(
            'conditions' => array(
                'Category.id' => $id
            )
        ));
        if(!$categories){
            $this->Session->setFlash('record not found', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            $this->redirect(array(
                "action" => 'indexCategory'
            ));
        }
        $this->set('category',$categories[0]);
    }
    public function deleteCategory($id = null){
    $categories = $this->Category->findById($id);
        if($categories){
            $this->Category->id = $id;
            $this->Category->saveField('del','0');
            $this->systemLogger('0303', $id);
            $this->Session->setFlash('record deleted', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            $this->redirect(array(
                "controller" => "posts",
                "action" => "indexCategory"
            ));
        }
        else{
            $this->Session->setFlash('record not found', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            $this->redirect(array(
                "controller" => "post",
                "action" => "indexCategory"
            ));
        }
    }
}
