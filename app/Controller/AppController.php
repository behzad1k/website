<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $uses = array('Log','Product','Post', 'User','Group','Category');
    public $components = array(
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'users',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'login'
                )

        ),
        'Session',
        'Cookie',
        'JDate',
        'RayaUpload',
        'Paginator'
    );
    public $theme = 'Adminlab';
    public $layout = 'panel';
    public $userLoggedIn = NULL;
    public $pageOptions = array();
    public $options = array(
        'styles' => array('/theme/Adminlab/assets/bootstrap-rtl/css/bootstrap-rtl.min.css','/theme/Adminlab/assets/bootstrap-rtl/css/bootstrap-responsive-rtl.min.css','/theme/Adminlab/assets/font-awesome/css/font-awesome.css','style.css','style_responsive.css','style_default.css'),
        'scripts' => array('jquery-1.8.3.min.js','/theme/Adminlab/assets/bootstrap-rtl/js/bootstrap.min.js','jquery.blockui.js','/theme/Adminlab/assets/chosen-bootstrap/chosen/chosen.jquery.min.js','/theme/Adminlab/assets/uniform/jquery.uniform.min.js','scripts.js')
    );
    public function systemLogger($action,$option = null){
        if($this->userLoggedIn['id']) {
            $userId = $this->userLoggedIn['id'];
        }
        else {
            $userId = 2;
        }

        $logData = array(
            'Log' => array(
                'user_id' => $userId,
                'action' => $action,
                'op' => $option,
                'ip' => $_SERVER['REMOTE_ADDR']
            )
            );
            $this->Log->create();
            $this->Log->save($logData);
    }
    public function beforeFilter() {
        parent::beforeFilter();
        $this->userLoggedIn = $this->Auth->user();
		$this->Auth->allow('index', 'view','add');
    }
    protected function panel(){
        $this->set('site',$this->options);
    }
}
