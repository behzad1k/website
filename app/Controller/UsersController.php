<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

App::uses('AppController', 'Controller');

/**
 * CakePHP UserController
 */
class UsersController extends AppController {
    public $paginate = array(
        'maxlimit' => 1,
        'limit' => 1,
	);
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login');
    }
    public function index($options = null) {
        $paginate = array('limit'=> 1,
            'order' => array(
                'User.id' => 'asc'
            ),'direction' => 'dsc');
        $this->Paginator->settings = $this->paginate;
        $this->pageOptions = array('pageTitle' => 'فهرست کاربران',
            'breadCrumbs' => array('کاربران', 'فهرست'));
        $this->set('pageOptions',$this->pageOptions);
        array_push($this->options['scripts'],'/theme/Adminlab/assets/data-tables/jquery.dataTables.js','/theme/Adminlab/assets/data-tables/DT_bootstrap.js');
        $users = $this->paginate('User');
        $this->set('users', $users);
        $this->panel();
    }
    public function login(){
		$this->theme = 'Woodwork';
		$this->layout = 'login';
        $user = $this->userLoggedIn;
        if($user){
        	if($user['group_id'] == 1){
				$this->redirect(array(
					'controller' => 'users',
					'action' => 'profile',
					$user['id']
				));
			}else{
				$this->redirect(array(
					'controller' => 'pages',
					'action' => 'home'
				));
			}

        }
        if($this->request->is('post')) {
			if ($this->request->data['User']['formsent'] == 'register') {
				$this->User->create();
				if($this->User->save($this->request->data)){
					$this->systemLogger('0101', $this->User->getInsertID());
					$this->redirect(array(
						'controller' => 'users',
						'action' => 'login'
					));
				}
				else {
					$this->Session->setFlash('اطلاعات نامعتبر', 'default', array(
						'class' => 'alert alert-important'
					));
				}
			} else {
				if ($this->Auth->login()) {
					$user = $this->Auth->user();
					$this->systemLogger('0100', $user['id']);
					if ($user['group_id'] == 1) {
						$this->Session->setFlash('Welcome ' . $user['name'], 'default', array(
							'class' => 'alert alert-success'
						));
						$this->redirect(array(
							"controller" => "users",
							"action" => "profile",
							$user['id']
						));
					} else {
						$this->redirect(array(
							'controller' => 'pages',
							'action' => 'home'
						));
					}
				} else {
					$this->Session->setFlash('Invalid Username Or Password', 'default', array(
						'class' => 'alert alert-important'
					));
					$this->redirect(array(
						"controller" => "users",
						"action" => "login"
					));
				}
			}
		}
        $options = array(
            'styles' => array('/theme/Adminlab/assets/bootstrap-rtl/css/bootstrap-rtl.min.css','/theme/Adminlab/assets/font-awesome/css/font-awesome.css','style.css','style_responsive.css','style_default.css'),
            'scripts' => array('jquery-1.8.3.min.js','/theme/Adminlab/assets/bootstrap-rtl/js/bootstrap.min.js','jquery.blockui.js','scripts.js')
        );
        $this->set('site',$options);
    }
    public function logout() {
        $this->Session->setFlash('loged out', 'default', array(
            'class' => 'alert alert-success'
        ));
        $this->Auth->logout();
        $this->redirect(array(
            "controller" => "users",
            "action" => "login"
        ));
    }
    public function add(){
        $this->pageOptions = array('pageTitle' => 'افزودن کاربر',
            'breadCrumbs' => array('کاربران', 'افزودن'));
        $this->set('pageOptions',$this->pageOptions);
        if($this->request->is('post')){

            $error = $this->checkError();
            if(!$error['hasError']){
                $this->User->create();
                if($this->User->save($this->request->data)){

					$this->systemLogger('0101', $this->User->getInsertID());
                    $this->Session->setFlash('record added', 'default', array(
                        'class' => 'alert alert-success'
                    ));
                    $this->redirect(array(
                        "controller" => "users",
                        "action" => "add"
                    ));
                }
                else {
                    $this->Session->setFlash('record not added', 'default', array(
                        'class' => 'alert alert-important'
                    ));
                }
            }else{
                $this->Session->setFlash($error['messageError'], 'default', array(
                    'class' => 'alert alert-important'
                ));
            }
        }
        $groupOption = $this->fetchGroup();
        array_push($this->options['styles'],'/theme/Adminlab/assets/bootstrap-datepicker/css/datepicker.css','/theme/Adminlab/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css');
        array_push($this->options['scripts'], 'js/jquery-1.8.2.min.js','js/jquery.blockui.js','/theme/Adminlab/assets/bootstrap-datepicker/js/bootstrap-datepicker.js','/theme/Adminlab/assets/bootstrap-daterangepicker/date.js','/theme/Adminlab/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js');
        $this->set('groupOption',$groupOption);
        $this->panel();

    }
    public function edit($id=null){
        $this->pageOptions = array('pageTitle' => 'ویرایش کاربر',
            'breadCrumbs' => array('کاربران', 'ویرایش'));
        $this->set('pageOptions',$this->pageOptions);
        array_push($this->options['styles'],'/theme/Adminlab/assets/bootstrap-datepicker/css/datepicker.css','/theme/Adminlab/assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css');
        array_push($this->options['scripts'], 'js/jquery-1.8.2.min.js','js/jquery.blockui.js','/theme/Adminlab/assets/bootstrap-datepicker/js/bootstrap-datepicker.js','/theme/Adminlab/assets/bootstrap-daterangepicker/date.js','/theme/Adminlab/assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js');
        $this->panel();
        if($this->request->is('post')){
            $this->User->id = $id;
            if($this->User->save($this->request->data)){
                $this->systemLogger('0102', $id);
                $this->Session->setFlash('record updated', 'default', array(
                        'class' => 'alert alert-success'
                    ));
                $this->redirect(array(
                    "controller" => "users",
                    "action" => "index"
                ));
            }
            else {
                 $this->Session->setFlash('record not updated', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            }
        }
        $users = $this->User->find("all",array(
            'conditions' => array(
                'User.id' => $id
            )
        ));
        if(!$users){
            $this->Session->setFlash('record not found', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            $this->redirect(array(
                "action" => 'index'
            ));
        }
        $this->set('user',$users[0]);
        $groupOption = $this->fetchGroup();
        $this->set('groupOption',$groupOption);
        }
    public function delete($id=null){
        $user = $this->User->findById($id);
        if($user){
            $this->User->id = $id;
            $this->User->saveField('del','0');
            $this->systemLogger('0103', $id);
            $this->Session->setFlash('record deleted', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            $this->redirect(array(
                "controller" => "users",
                "action" => "index"
            ));
        }
        else{
            $this->Session->setFlash('record not found', 'default', array(
                        'class' => 'alert alert-success'
                    ));
            $this->redirect(array(
                "controller" => "user",
                "action" => "index"
            ));
        }
    }
    public function changepassword($id = null){
        $this->pageOptions = array('pageTitle' => 'ویرایش رمز',
            'breadCrumbs' => array('کاربران', 'ویرایش رمز'));
        $this->set('pageOptions',$this->pageOptions);
        $this->panel();
        $this->set('currentAddress',$this->currrentAddress);
        $user = $this->User->find("all",array(
            'conditions' => array(
                'User.id' => $id
            )
        ));
        if($this->request->is('post')){
            if($user){
                $this->User->id = $id;
                if($user[0]['User']['id'] == $this->userLoggedIn['id']){
                    $passHash = new SimplePasswordHasher();
                    $oldpass = $passHash->hash($this->request->data['User']['oldpassword']);
                    $oldpass2 = $user[0]['User']['password'];
                    if($oldpass == $oldpass2){
                        if(!$this->checkError('password')['hasError']){
                            $this->User->saveField('password',$this->request->data['User']['password']);
                            $this->Session->setFlash('Password Changed', 'default', array(
                            'class' => 'alert alert-success'
                        ));
                $this->redirect(array(
                    "controller" => "users",
                    "action" => "profile"
                ));
                        }
                    }

                }
                elseif ($user[0]['User']['group_id'] == '1'){
                    if(!$this->checkError('password')['hasError']){
                            $this->User->saveField('password',$this->request->data['User']['password']);
                            $this->Session->setFlash('Password Changed', 'default', array(
                            'class' => 'alert alert-success'
                        ));
                $this->redirect(array(
                    "controller" => "users",
                    "action" => "profile"
                ));
                    }
                }
                else{
                    $this->Session->setFlash('Invalid request', 'default', array(
                            'class' => 'alert alert-important'
                        ));
                }
            }
        }
            $this->set('superuser',$this->userLoggedIn['id']);
            $this->set('userId',$user[0]['User']['id']);
    }
    public function profile($id=null){
        $this->pageOptions = array('pageTitle' => 'پروفایل کاربران ',
            'breadCrumbs' => array('کاربران', 'پروفایل'));
        $this->set('pageOptions',$this->pageOptions);
        $this->panel();
        $users = $this->User->find('all',array(
            'conditions' => array(
                'User.id' => $id
            )
        ));
        $this->set('user',$users[0]);
        $this->set('currentAddress',$this->currrentAddress);
        if(!$users){
            $this->Session->setFlash('record not found', 'default');
            $this->redirect(array(
                "action" => 'index'
            ));
        }
    }
    protected function fetchGroup (){
        $groups = $this->Group->find('all');
        $groupArray = array();
        foreach ($groups as $group){
            $groupArray[$group['Group']['id']] = $group['Group']['title'];
        }
        return $groupArray;
    }
    public function checkError($option = null){
        $hasError = False;
        $messageError = null;
        if($option == null || $option == 'username'){
            $names = $this->User->find("all",array(
                'conditions' => array(
                    'User.username' => $this->request->data['User']['username']

            )));
            if($names){
                $hasError = true;
                $messageError = "نام کاربری تکراری است" . '<br>';
            }
        }
        if($option == null || $option == 'password'){
            if($this->request->data['User']['password'] != $this->request->data['User']['repassword']){
                $hasError = true;
                $messageError .= "تکرار گذرواژه صحیح نیست";
            }
            return array(
                'hasError' => $hasError,
                'messageError' => $messageError
            );
        }
    }
}
