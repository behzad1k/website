<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * CakePHP RayaUploadComponent
 * @author behzad1k
 */
class RayaUploadComponent extends Component {

    public $file;
    public $fileName;
    public $pathName;
    public $maxFileSize = 204800;
    public $allowReplaceFile = false;
    public $allowExtention = array('*');
    public $denyExtention = array('php','php4','php5','ctp','html','js','htm','css','bat','sh','exe','htaccess','asp','aspx','xml');
    // 1 = success
    // 2 = extention error
    // 3 = fileSize error
    // 4 = Unknown error
    private function checkError(){
        @$fileExtention = end(explode(".", $this->fileName));
        if(in_array($fileExtention, $this->denyExtention)){
            return 2;
        }
        if($fileExtention != '*'){
            if(!in_array($fileExtention, $this->allowExtention)){
                return 2;
            }
        }
        if(($this->file['size']/1024) > $this->maxFileSize){
            return 3;
        }
        return 1;
    }
    public function upload(){
        $checkError = $this->checkError();
        if($checkError == 1){
            if(move_uploaded_file($this->file['tmp_name'], $this->pathName)){
                return 1;
            }
            else{
                return 4;
            }
        }
        else {
            return $checkError;
        }
    }
}