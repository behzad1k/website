<!doctype html>
<html lang="en">

<head>
    <title>Shopper</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Style CSS-->
	<?= $this->Html->css('main.css?v=' . time()) ?>
    <!--java script (jquery)-->





</head>

<body>
    <!--Navigation-->
    <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container">
            <a class="navbar-brand" href="index.html">logo</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon">
				  <?=
				  	$this->Html->image('list.svg')
				  ?>
			  </span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">درباره ی ما</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#">ثبت سفارش</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#aboutus">ارتباط باما</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery.html">گالری محصولات</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-pill btn-primary" href="Login.ctp">
                             ثبت نام / ورود
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <!--Home screen-->
    <section class="home ">
        <div class="row m-5 mmm">
            <div class=" col-lg-2 col-md-4 my-5">
                <div class="card text-center shadow-lg">
					<?=
					$this->Html->image('p2.jpg',array(
							"class" => 'card-img-top',
							"alt" => "dress"
					))
					?>
                    <div class="card-body">
                        <h5 class="card-title">میز</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 my-5">
                <div class="card text-center shadow-lg ">
                    <img class="card-img-top" src="img/p3.jpg" alt="Dress">
                    <div class="card-body">
                        <h5 class="card-title">صندلی</h5>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-4 my-5">
                <div class="card text-center shadow-lg">
                    <img class="card-img-top" src="img/p4.jpg" alt="Dress">
                    <div class="card-body">
                        <h5 class="card-title">کمد</h5>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 text-right d-flex flex-column justify-content-around align-items-around text-color">
                <h1>یه شعار خفن یا اسم کمپانی</h1>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است</p>
            </div>
        </div>

    </section>
    <!--Call to action -->

    <section class="call-to-action py-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-right">
                    <h3 class="call-to-action-heading">ما چه جوری انجامش میدیم؟</h3>
                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است</p>
                </div>
                <div class="col-lg-4">
                    <button class="btn btn-lg btn-outline-secondary mt-4">بیشتر بخوانید</button>
                </div>
            </div>
        </div>
    </section>
    <!--Gallery-->
    <section class="shop my-5 ">
        <div class="container">
            <h2 class="section-heading">جدید ترین محصولات</h2>
            <div class="d-flex justify-content-center">
                <button type="button" class="btn btn-primary filter" data-rel="all">همه</button>
                <button type="button" class="btn btn-primary filter" data-rel="1"> گروه اول</button>
                <button type="button" class="btn btn-primary filter" data-rel="2">گروه دوم</button>
            </div>
            <div class="gallery mt-5">
                <div class="mb-3 pics all 2">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p1.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">29</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 1">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p2.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">29</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 1">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p3.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 2">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p4.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 2">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p5.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">29</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 2">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p6.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">39</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 2">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p7.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 1">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p8.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 1">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p9.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 1">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p10.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 1">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p11.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 1">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p12.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 1">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p13.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 1">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p14.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
                <div class="mb-3 pics  all 1">
                    <a href="detail.html">
                        <img class="img-fluid" src="img/p15.jpg" alt="Card image cap">
                        <p class="price-tag rounded-circle"><span class="price">19</span><span class="rial">ریال</span></p>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!--line-->

    <div class="line"></div>
    <!--Contact US-->
    <section class="contact bg-light py-5" >
        <div class="container ">
            <h2 class="section-heading" id="aboutus">ارتباط با ما</h2>
            <form class="col-lg-6 offset-lg-3 text-center">
                <div class="form-group">
                    <label for="email">ایمیل</label>
                    <input type="email" id="email" class="form-control" aria-describedby="emailHelp" placeholder="ایمیل خود را وارد کنید">
                    <small id="emailHelp" class="form-text text-muted">ایمیل شما به اشتراک گذاشته نمیشود</small>
                </div>
                <div class="form-group">
                    <label for="name">نام </label>
                    <input type="text" id="name" class="form-control" placeholder="نام خود را وارد کنید">
                </div>
                <div class="form-group">
                    <label for="message">پیام شما:</label>
                    <textarea class="form-control" id="message" placeholder="Type your message" row="7">

                </textarea>
                </div>
                <div class="text-center">
                    <button class="btn btn-lg btn-primary">ارسال</button>
                </div>
            </form>
        </div>
    </section>
    <!--Footer-->
    <footer class="footer py-5">
        <div class="container">
           <h1>یه مشت کلمه</h1>
        </div>
    </footer>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="jquery.js"></script>
</body>

</html>
