<!doctype html>
<html lang="en">

<head>
	<title>title</title>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- Style CSS-->
	<?= $this->Html->css('main.css?v=' . time()) ?>



</head>

<body>


<!--Navigation-->
<nav class="navbar navbar-expand-lg fixed-top">
	<div class="container">
		<a class="navbar-brand" href="index.html">logo</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"><img src="img/list.svg" alt=""></span>
		</button>
		<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
			<ul class="nav">
				<li class="nav-item">
					<?= $this->Html->link('درباره ی ما',array(
							'controller' => 'pages',
							'action' => 'aboutUs'
					),array('class' => 'nav-link active')) ?>
				</li>
				<li class="nav-item">
					<?= $this->Html->link('ثبت سفارش',array(
							'controller' => 'pages',
							'action' => 'placeOrder'
					),array(
							'class' => 'nav-link'
					)) ?></li>
				<li class="nav-item">

					<?= $this->Html->link('ارتباط با ما',array(
							'controller' => 'pages',
							'action' => 'placeOrder'
					),array(
							'class' => 'nav-link'
					)) ?>				</li>
				<li class="nav-item">
					<?= $this->Html->link('گالری محصولات',array(
							'controller' => 'pages',
							'action' => 'gallery'
					),array(
							'class' => 'nav-link'
					)) ?>				</li>
				<li class="nav-item">
					<?= $this->Html->link('ثبت نام / ورود',array(
							'controller' => 'users',
							'action' => 'login'
					),array('class' => 'nav-link btn btn-primary rounded')) ?>
				</li>
			</ul>
		</div>
	</div>
</nav>
<!--line-->

<div class="line2"></div>

<?= $this->Session->flash(); ?>
<?= $this->fetch('content') ?>
<!--news-->

<div class="container d-flex flex-column align-items-center">
	<div id="demo" class="carousel slide container" data-ride="carousel">

		<!-- Indicators -->

		<!-- The slideshow -->
		<div class="carousel-inner">
			<?php
			$index = 0;
			foreach ($posts as $post){
			if($post['Post']['del'] == '1'){
			if($index == 0){?>
			<div class="carousel-item active">
				<?php }else{?>
				<div class="carousel-item ">
					<?php } ?>
					<div class="text-center d-flex flex-column justify-items-center align-items-center p-5"
						 style="width: 100%; height: 100%;">
						<h3 class="m-5"><?= $post['Post']['title'] ?></h3>
						<p class="mr-5 ml-5 mb-5  text-color3">
							<?= $post['Post']['text']?>
						</p>
						<?=$this->Html->link($this->Form->button('بیشتر بخوانید',array('class' => "btn btn-lg mt-4 btn-primary")), array('action' => 'singlePost',$post['Post']['id']), array('escape'=>false,'title' => "Click to view somethin"));?>
					</div>
				</div>
				<?php $index++;} }?>

				<button class="btn btn-lg mt-4 btn-primary  more">مشاهده ی همه</button>
			</div>
		</div>



		<!-- Left and right controls -->
		<a class="carousel-control-prev" href="#demo" data-slide="prev">
			<span class="carousel-control-prev"> <img src="img/arrow-l.svg" alt="prev" style="width: 50px; height: 50px;"></span>
		</a>
		<a class="carousel-control-next" href="#demo" data-slide="next">
			<span class="carousel-control-next"> <img src="img/arrow-r.svg" alt="next" style="width: 50px; height: 50px;"></span>
		</a>
	</div>
</div>

<!--line-->

<div class="line2"></div>
<!--Footer-->

<div class="footer2">
	<div class="container ">
		<div class="container d-flex justify-content-between " >
			<div class="footer1 d-flex flex-column justify-content-center align-items-center">
				<p class="title">شماره تماس</p>
				<p class="child" style="direction: ltr;">+98 980 9898</p>

				<p class="title"> شماره دفتر</p>
				<p class="child" style="direction: ltr;">021 2277788</p>
			</div>


			<div class="footer1 d-flex flex-column justify-content-center align-items-center">
				<p class="title"> پشتیبانی سایت</p>
				<p class="child" style="direction: ltr;">something@gmail.com</p>
				<p class="child text-right">درصورتی که با مشکل فنی در سایت مواجه شدید با ما در میان بگذارید تا سریعا نسبت به رفع آن اقدام شود.باتشکر</p>
			</div>
		</div>
		<div class="line2"></div>
		<p class="text-right" >*لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ</p>
	</div>

</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
<?= $this->Html->script('jquery.js') ?>
</body>

</html>
