 	<!doctype html>
<html lang="en">

<head>
	<title><?= $pageOptions['title'] ?></title>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- Style CSS-->
	<?= $this->Html->css('main.css?v=' . time()) ?>



</head>

<body>


<!--Navigation-->
<nav class="navbar navbar-expand-lg fixed-top">
	<div class="container">
		<a class="navbar-brand" href="index.html">logo</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"><img src="img/list.svg" alt=""></span>
		</button>
		<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
			<ul class="nav">
				<li class="nav-item">
					<?= $this->Html->link('درباره ی ما',array(
							'controller' => 'pages',
							'action' => 'aboutUs'
					),array('class' => 'nav-link active')) ?>
				</li>
				<li class="nav-item">
					<?= $this->Html->link('ثبت سفارش',array(
							'controller' => 'pages',
							'action' => 'placeOrder'
					),array(
							'class' => 'nav-link'
					)) ?></li>
				<li class="nav-item">

				<?= $this->Html->link('ارتباط با ما',array(
							'controller' => 'pages',
							'action' => 'placeOrder'
					),array(
							'class' => 'nav-link'
					)) ?>				</li>
				<li class="nav-item">
					<?= $this->Html->link('گالری محصولات',array(
							'controller' => 'pages',
							'action' => 'gallery'
					),array(
							'class' => 'nav-link'
					)) ?>				</li>
				<li class="nav-item">
					<?php if($pageOptions['user']){ ?>
						<?= $this->Html->link('خروج',array(
								'controller' => 'users',
								'action' => 'logout'
						),array('class' => 'nav-link btn btn-primary rounded')) ?>
					<?php }else{ ?>
						<?= $this->Html->link('ثبت نام / ورود',array(
								'controller' => 'users',
								'action' => 'login'
						),array('class' => 'nav-link btn btn-primary rounded')) ?>
					<?php } ?>
				</li>
			</ul>
		</div>
	</div>
</nav>

<!--Home screen-->
<section class="portfolio container-fluid bg-light">
	<div class=" py-5">

	</div>
	<div class="row mb-5">
		<div class="col-md-3 p-0">
			<div class="filter"></div>
			<?= $this->Html->image('p9.jpg',array('class' => 'img-fluid'))?>
		</div>
		<div class="col-md-3 p-0 dark d-flex align-items-center justify-content-center">
			<div class="text-center">
				<h4 class="py-3">لورم ایپسوم</h4>

			</div>
		</div>
		<div class="col-md-3 p-0">
			<div class="filter"></div>
			<?= $this->Html->image('p11.jpg',array('class' => 'img-fluid'))?>		</div>
		<div class="col-md-3 p-0 dark d-flex align-items-center justify-content-center">
			<div class="text-center">
				<h4 class="py-3">لورم ایپسوم</h4>

			</div>
		</div>
		<div class="col-md-3 p-0 dark d-flex align-items-center justify-content-center">
			<div class="text-center">
				<h4 class="py-3">لورم ایپسوم</h4>

			</div>
		</div>
		<div class="col-md-3 p-0">
			<div class="filter"></div>
			<?= $this->Html->image('p8.jpg',array('class' => 'img-fluid'))?>		</div>
		<div class="col-md-3 p-0 dark d-flex align-items-center justify-content-center">
			<div class="text-center">
				<h4 class="py-3">لورم ایپسوم</h4>

			</div>
		</div>
		<div class="col-md-3 p-0">
			<div class="filter"></div>
			<?= $this->Html->image('p6.jpg',array('class' => 'img-fluid'))?>
			</div>
	</div>
</section>
<!--how we do it -->

<section class="call-to-action py-3">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 text-right">
				<h3 class="call-to-action-heading">ما چه جوری انجامش میدیم؟</h3>
				<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است</p>
			</div>
			<div class="col-lg-4">
				<button class="btn btn-lg btn-outline-secondary mt-4">بیشتر بخوانید</button>
			</div>
		</div>
	</div>
</section>

<!--Gallery-->

<!--***********************************************************************************-->


<section class=" my-5 ">
	<div class="container mt-5">
		<h2 class="section-heading">جدید ترین محصولات </h2>
		<div class="d-flex justify-content-center">
			<button type="button" class="btn btn-primary" data-rel="all">همه</button>
			<button type="button" class="btn btn-primary" data-rel="1"> گروه اول</button>
			<button type="button" class="btn btn-primary" data-rel="2">گروه دوم</button>
		</div>
		<div class="gallery mt-5">
			<?php foreach ($products as $item){?>
				<div class="pro-box">
					<div class="card text-center shadow-lg all 2 ">
						<?php
						if($item['Product']['picture']) {
							$imgName = $productsImagesPath . $item['Product']['id'];
							$img = '<img class="card-img-top" src="' . $this->webroot . $productsImagesPath . $item['Product']['id'] .'" alt="'.$item['Product']['title'].'" >';
						}


						else {
							//				$imgFile = 'noimage-1.jpg';
							//				$img = $this->Html->image($imgFile, array(
							//						'class' => 'res-img',
							//						'alt' => $item['Product']['title'],
							//
							//				));
							$img = NULL;

						}
						echo $this->Html->link($img, array(
								'controller' => 'Pages',
								'action' => 'detail',
								$item['Product']['id']
						), array(
								'escape' => false
						));
						?>
						<div class="text-center ">
							<h4><?= $item['Product']['title'] ?></h4>
							<p><?= $item['Product']['info'] ?></p>
							<p><?= $item['Product']['price'] ?></p>
						</div>
					</div>
				</div>
			<?php } ?>

		</div>


	</div>


</section>
<!--line-->

<div class="line2"></div>

<!--news-->

<div class="container d-flex flex-column align-items-center">
	<div id="demo" class="carousel slide container" data-ride="carousel">

		<!-- Indicators -->

		<!-- The slideshow -->
		<a class="carousel-control-prev" href="#demo" data-slide="prev">

			<span class="carousel-control-prev">
				<?= $this->Html->image('arrow-l.svg', array('alt' => 'prev','class' => 'card-img-top','style' => "width:50px;height:50px")) ?>
</span>
		</a>

		<!-- Left and right controls -->
		<a class="carousel-control-next" href="#demo" data-slide="next">
			<span class="carousel-control-next">
				<?= $this->Html->image('arrow-r.svg', array('alt' => 'next','class' => 'card-img-top','style' => "width:50px;height:50px")) ?>
			</span>
		</a>
		<div class="carousel-inner">
			<?php
			$index = 0;
			foreach ($posts as $post){
				if($post['Post']['del'] == '1'){
					if($index == 0){?>
				<div class="carousel-item active">
					<?php }else{?>
						<div class="carousel-item ">
					<?php } ?>
				<div class="text-center d-flex flex-column justify-items-center align-items-center p-5"
					 style="width: 100%; height: 100%;">
					<h3 class="m-5"><?= $post['Post']['title'] ?></h3>
					<p class="mr-5 ml-5 mb-5  text-color3">
						<?= $post['Post']['text']?>
					</p>
					<?=$this->Html->link($this->Form->button('بیشتر بخوانید',array('class' => "btn btn-lg mt-4 btn-primary")), array('action' => 'singlePost',$post['Post']['id']), array('escape'=>false,'title' => "Click to view somethin"));?>
				</div>
			</div>
				<?php $index++;} }?>

	<button class="btn btn-lg mt-4 btn-primary  more">مشاهده ی همه</button>
</div>

<!--line-->

<div class="line2"></div>
<!--contact us-->
<section class="contact">
	<div class="container">
		<div class="text-center py-5">
			<h2 class="section-heading py-3" id="aboutus">ارتباط با ما</h2>
			<div class="mx-auto heading-line"></div>
		</div>
		<div class="row">



			<form class="col-lg-6 text-center">
				<div class="form-group">
					<label for="email">ایمیل</label>
					<input type="email" id="email" class="form-control" aria-describedby="emailHelp"
						   placeholder="ایمیل خود را وارد کنید">
					<small id="emailHelp" class="form-text text-muted">ایمیل شما به اشتراک گذاشته نمیشود</small>
				</div>
				<div class="form-group">
					<label for="name">نام </label>
					<input type="text" id="name" class="form-control" placeholder="نام خود را وارد کنید">
				</div>
				<div class="form-group">
					<label for="message">پیام شما:</label>
					<textarea class="form-control" id="message" placeholder="Type your message" row="7">

                </textarea>
				</div>
				<div class="text-center">
					<button class="btn btn-lg btn-primary">ارسال</button>
				</div>

			</form>




			<div class="col-lg-6">
				<!--Google map-->
				<div id="map-container-google-1" class="z-depth-1-half map-container">
					<iframe src="https://maps.google.com/maps?q=manhatan&t=&z=13&ie=UTF8&iwloc=&output=embed"
							frameborder="0" style="border:0; width:100%; height:420px" allowfullscreen></iframe>
				</div>
				<!--Google Maps-->
			</div>
		</div>
	</div>
</section>
<!--Footer-->

<div class="footer2">
	<div class="container ">
		<div class="container d-flex justify-content-between " >
			<div class="footer1 d-flex flex-column justify-content-center align-items-center">
				<p class="title">شماره تماس</p>
				<p class="child" style="direction: ltr;">+98 980 9898</p>

				<p class="title"> شماره دفتر</p>
				<p class="child" style="direction: ltr;">021 2277788</p>
			</div>


			<div class="footer1 d-flex flex-column justify-content-center align-items-center">
				<p class="title"> پشتیبانی سایت</p>
				<p class="child" style="direction: ltr;">something@gmail.com</p>
				<p class="child text-right">درصورتی که با مشکل فنی در سایت مواجه شدید با ما در میان بگذارید تا سریعا نسبت به رفع آن اقدام شود.باتشکر</p>
			</div>
		</div>
		<div class="line2"></div>
		<p class="text-right" >*لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ</p>
	</div>

</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
<?= $this->Html->script('jquery.js') ?>
</body>

</html>
