
<!--order form-->
<div class="container shadow d-flex flex-column align-items-center" style="margin-top: 100px;">
	<h3 class="mt-5 text-right">برای ثبت سفارش یا شخصی سازی ، موارد مورد نیاز از لیست زیر را با دقت پرکنید</h3>
		<div>
			<?= $this->Form->create('Order') ?>
			<p class="section-heading ">عنوان سفارش مد نظر خود را از این قسمت وارد کنید</p>
			<?= $this->Form->input('title',array(
				'type' => 'text',
				'class' => 'input-box',
				'label' => FALSE,
				'div' => FALSE,
				'placeholder' => 'عنوان'
			)) ?>
		</div>

		<div class="line2 m-5"></div>
		<div style="direction: ltr;" class="text-center mt-2">
			<p class="section-heading ">ابعاد مورد نظر خود را وارد کنید</p>
			<div class="cc">

				<?= $this->Form->input('length',array(
					'type' => 'text',
					'class' => 'input-box',
					'label' => FALSE,
					'div' => FALSE,
					'placeholder' => 'طول'
				)) ?>cm <br>

				<?= $this->Form->input('width',array(
					'type' => 'text',
					'label' => FALSE,
					'div' => FALSE,
					'class' => 'input-box',
					'placeholder' => 'عرض'
				)) ?>cm <br>

				<?= $this->Form->input('diameter',array(
					'type' => 'text',
					'class' => 'input-box',
					'label' => FALSE,
					'div' => FALSE,
					'placeholder' => 'قطر'
				)) ?>cm <br>

				<?= $this->Form->input('height',array(
					'type' => 'text',
					'class' => 'input-box',
					'label' => FALSE,
					'div' => FALSE,
					'placeholder' => 'ارتفاع'
				)) ?>cm <br>
			</div>
		</div>

		<div class="line2 m-5"></div>
		<div>
			<p class="section-heading ">نوع چوب</p>
			<input type="radio" id="" name="" value="">
			<label for=""> راش</label>
			<input type="radio" id="" name="" value="">
			<label for=""> افرا</label>
			<input type="radio" id="" name="" value="">
			<label for=""> گردو</label>
			<input type="radio" id="" name="" value="">
			<label for=""> mdf</label>
		</div>

		<div class="line2 m-5"></div>
		<div>
			<p class="section-heading ">اگر نیاز به اضافه کردن اطلاعات دیگر یا توضیحات دارید از این قسمت استفاده کنید</p>
			<?= $this->Form->input('info',array(
				'type' => 'textarea',
				'class' => 'form-control',
				'label' => FALSE,
				'div' => FALSE,
				'row' => 7
			)) ?>
			<div class="text-center">
				<button type="submit" class="btn btn-lg mt-4 btn-primary">ثبت سفارش</button>
				<?= $this->Form->end() ?>
			</div>
		</div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>

</html>
