<body>
<!--Gallery-->

<!--***********************************************************************************-->

<section style="margin-top: 200px;">
	<div class="container mt-5">
		<h2 class="section-heading"> محصولات </h2>
		<div class="d-flex justify-content-center">
			<button type="button" class="btn btn-primary filter" data-rel="all">همه</button>
			<button type="button" class="btn btn-primary filter" data-rel="1"> گروه اول</button>
			<button type="button" class="btn btn-primary filter" data-rel="2">گروه دوم</button>

		</div>
		<form action="#" class="search-g mx-auto" >

			<input type="search"  class="search-box " name="search " placeholder=" دنبال چه محصولی میگردید؟">

			<input type="submit"  class="search-btn btn " value="جستجو">
		</form>
		<div class="gallery mt-5">
			<?php foreach ($products as $item){?>
			<div class="pro-box">
				<div class="card text-center shadow-lg all 2 ">
					<?php
					if($item['Product']['picture']) {
						$imgName = $productsImagesPath . $item['Product']['id'];
						$img = '<img class="card-img-top" src="' . $this->webroot . $productsImagesPath . $item['Product']['id'] .'" alt="'.$item['Product']['title'].'" >';
					}


					else {
		//				$imgFile = 'noimage-1.jpg';
		//				$img = $this->Html->image($imgFile, array(
		//						'class' => 'res-img',
		//						'alt' => $item['Product']['title'],
		//
		//				));
						$img = NULL;

					}
					echo $this->Html->link($img, array(
							'controller' => 'Pages',
							'action' => 'detail',
							$item['Product']['id']
					), array(
							'escape' => false
					));
					?>
							<div class="text-center ">
								<h4><?= $item['Product']['title'] ?></h4>
								<p><?= $item['Product']['info'] ?></p>
								<p><?= $item['Product']['price'] ?></p>
							</div>
						</div>
					</div>
					<?php } ?>
		</div>



</section>

</body>
