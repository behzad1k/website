<body>


<!-- login form -->
<div class="body">
	<div class="container" id="container">
		<div class="form-container sign-up-container">
				<?= $this->Form->create('User') ?>
				<h1 style="color: #c5a880;">ثبت نام</h1>
				<?= $this->Form->input('name', array(
						'type' => 'text',
						'label' => FALSE,
						'div' => FALSE,
						'placeholder' => 'نام'
				))?>

				<?= $this->Form->input('email', array(
						'type' => 'text',
						'label' => FALSE,
						'div' => FALSE,
						'placeholder' => 'ایمیل'
				))?>

				<?= $this->Form->input('password', array(
						'type' => 'password',
						'label' => FALSE,
						'div' => FALSE,
						'placeholder' => 'رمزعبور'
				))?>

				<?= $this->Form->input('phone', array(
						'type' => 'text',
						'label' => FALSE,
						'div' => FALSE,
						'placeholder' => 'شماره موبایل'
				))?>
				<?= $this->Form->hidden('formsent', array('value' => 'register'));?>
				<button class="button">ثبت نام</button>
				<button class="btn-responsive shadow button" id="btn2">ورود</button>
				<?= $this->Form->end() ?>
		</div>
		<div class="form-container sign-in-container">
				<?= $this->Form->create('User') ?>
			<h1 style="color: #c5a880;">ورود به حساب کاربری</h1>
			<?= $this->Form->input('email', array(
						'type' => 'text',
						'label' => FALSE,
						'div' => FALSE,
						'placeholder' => 'نام کاربری'
				))?>

				<?= $this->Form->input('password', array(
						'type' => 'password',
						'label' => FALSE,
						'div' => FALSE,
						'placeholder' => 'رمزعبور'
				))?>
				<?= $this->Form->hidden('formsent', array('value' => 'login'));?>
				<a class="a" href="#">رمز عبور خود را فراموش کردید؟</a>
				<button class="button">ورود</button>
				<button class="btn-responsive shadow button" id="btn1">ثبت نام</button>
				<?= $this->Form->end() ?>
		</div>
		<div class="overlay-container">
			<div class="overlay">
				<div class="overlay-panel overlay-left">
					<h1>خوش آمدید</h1>
					<p class="p">برای ارتباط با ما ، لطفا با اطلاعات شخصی خود وارد شوید</p>
					<button class="ghost button" id="signIn">ورود</button>
				</div>
				<div class="overlay-panel overlay-right">
					<h1>سلام دوست عزیز</h1>
					<p class="p">برای پیوستن به ما از این کزینه استفاده کنید</p>
					<button class="ghost button" id="signUp">ثبت نام</button>

				</div>
			</div>
		</div>
	</div>

</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
<?= $this->Html->script('login-jq.js') ?>


</body>
