<!doctype html>
<html lang="en">
<head>
	<title>Title</title>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<?= $this->Html->css('main.css?v=' . time()) ?>


</head>


<body>

<!--  <div class="back-color"></div>   -->
<div class="main p-5 text-right">
	<?= $this->Html->image('aboutus.jpg',array('style' => "width : 100%;height : auto")) ?>

	<h1><?= $post['Post']['title'] ?></h1>

	<p>
		<?= $post['Post']['text'] ?>
	</p>


</div>


<!--related products-->

<div class=" my-3 part1">
	<h2 class="section-heading"> جدید ترین محصولات</h2>
	<div id="demo0" class="carousel slide container" data-ride="carousel">

		<!-- Indicators -->

		<!-- The slideshow -->

		<!-- Left and right controls -->
		<a class="carousel-control-prev" href="#demo0" data-slide="prev">
       <span class="carousel-control-prev"> <img src="img/arrow-l.svg" alt="prev"
												 style="width: 50px; height: 50px;"></span>
		</a>
		<a class="carousel-control-next" href="#demo0" data-slide="next">
       <span class="carousel-control-next"> <img src="img/arrow-r.svg" alt="next"
												 style="width: 50px; height: 50px;"></span>
		</a>
	</div>

</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
