
<!DOCTYPE html>
<!--
Template Name: Admin Lab Dashboard build with Bootstrap v2.3.1
Template Version: 1.0
Author: Mosaddek Hossain
Website: http://thevectorlab.net/
-->

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <title>Login page</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
   <?= $this->Html->css($site['styles']); ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body id="login-body">
  <div class="login-header">
      <!-- BEGIN LOGO -->
      <div id="logo" class="center">
          <img src="img/logo.png" alt="logo" class="center" />
      </div>
      <!-- END LOGO -->
  </div>
      <?= $this->Session->flash(); ?>
      <?= $this->fetch('content') ?>
  <!-- BEGIN COPYRIGHT -->
  <div id="login-copyright">
      2013 &copy; Admin Lab Dashboard.
  </div>
  <!-- END COPYRIGHT -->
  <!-- BEGIN JAVASCRIPTS -->
       <?= $this->Html->script($site['scripts']); ?>
  <script>
      
    jQuery(document).ready(function() {     
      App.initLogin();
    });

  </script>
  <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>