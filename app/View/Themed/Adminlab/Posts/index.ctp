
<!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="widget">
                        <div class="widget-title">
                           <h4><i class="icon-globe"></i><?= $pageOptions['pageTitle']?></h4>
                        </div>
                            <div class="widget-body">
                        <?php if($posts): ?>
                                <table class="table table-striped table-bordered table-advance table-hover">
                           <thead>
                              <tr>
                                 <th><i class="icon-key"></i> Id</th>
                                 <th><i class="icon-book"></i> عنوان</th>
                                 <th><i class="icon-user"></i> نویسنده</th>
                                 <th><i class="icon-tags"></i> دسته بندی</th>
                                 <th><i class="icon-filter"></i> کلمات کلیدی</th>
                                 <th><i class="icon-bookmark"></i> شرح کوتاه</th>
                                 <th><i class="icon-bookmark"></i> تاریخ درج</th>
                                 <th><i class="icon-text-height"></i>متن</th>
                                 <th class="hidden-phone"></th>
                                 <th class="hidden-phone"></th>
                           </thead>
                           <tbody>
                               
                                   <?php foreach ($posts as $post){
                                       if($post['Post']['del'] == '1'){?>
                           <tr data-href="http://localhost/website/posts/add">
                                 <td class="highlight">
                                 <?= $post['Post']['id'] ?>
                                 </td>
                                 <td class="hidden-phone"> <?= $post['Post']['title'] ?></td>
                                 <td class="hidden-phone"> <?= $post['Post']['writer'] ?></td>
                                 <td></td>
                                 <td><?= $post['Post']['keywords'] ?></td>
                                 <td><?= $post['Post']['summary'] ?></td>
                                 <td><?php 
                                    $datetime = explode(' ',$post['Post']['created']);
                                    $date = explode('-',$datetime[0]);
                                    $jdate = $this->JDate->gregorian_to_jalali($date[0], $date[1], $date[2]);
                                    echo $jdate;
                                 ?>
                                 </td>
                                 <td><?= $post['Post']['text'] ?></td>
                                 <td><?= $this->Html->link('<i class="icon-edit"></i>',array(
                                     'action' => 'edit',
                                     $post['Post']['id']
                                 ),array(
                                     'escape' => false,
                                     'style' => array('padding-right'=>'20px')
                                     ))?></td>
                                 <td>
                                       <?=
                                       $this->Html->link('<i class="icon-trash"></i>', array(
                                         'action' => 'delete',
                                         $post['Post']['id']
                                     ),array(
                                         'escape' => false)) ?></td>
                            </tr>
                            <?php
                                       }
                                   }
                            ?>
                        </table>
                                <?php        endif; ?>

                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->  

