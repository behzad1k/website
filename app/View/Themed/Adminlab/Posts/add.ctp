<!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="widget">
                        <div class="widget-title">
                           <h4><i class="icon-globe"></i><?= $pageOptions['pageTitle']?></h4>
                        </div>
                        <div class="widget-body">
                            <?= $this->Form->create('Post',array(
                                'class' => 'form-horizontal',
                                'type' => 'file'
                            )) ?>
                            <div class="control-group">
                                <label class="control-label">عنوان</label>
                                <div class="controls">
                                    <?= $this->Form->input('title', array(
                                        'type' => 'text',
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">تصویر</label>
                                <div class="controls">
                                    <?= $this->Form->input('pic', array(
                                        'type' => 'file',
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">نویسنده</label>
                                <div class="controls">
                                    <?= $this->Form->input('writer', array(
                                        'type' => 'text',
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">کلمات کلیدی</label>
                                <div class="controls">
                                    <?= $this->Form->input('keywords', array(
                                        'type' => 'text',
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">دسته بندی</label>
                                <div class="controls">
                                    <?= $this->Form->input('Category', array(
                                        'multiple' => 'multiple',
                                        'options' => $catOptions,
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">شرح خبر</label>
                                <div class="controls">
                                    <?= $this->Form->input('summary', array(
                                        'type' => 'text',
                                        'rows' => '2',
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <!-- BEGIN FORM-->
                                <div class="control-group">
                                    <label class="control-label">CKEditor</label>
                                    <div class="controls">
                                        <?= $this->Form->textarea('text',array(
                                            'class' => 'span12 ckeditor',
                                            'name' => 'editor1',
                                            'rows' => '6',
                                            'id' => 'text',
                                            'readonly' => true,
                                            'dev' => false,
                                            'label' => false
                                        )) ?>
                                    </div>
                                </div>
                                <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
 	                            <!-- END FORM-->
                        </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->
