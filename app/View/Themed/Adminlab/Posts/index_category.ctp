
            <div class="row-fluid">
               <div class="span12">
                  <div class="widget">
                        <div class="widget-title">
                           <h4><i class="icon-globe"></i><?=$pageOptions['pageTitle']?></h4>

                        </div>
                        <div class="widget-body">
                            <?php if($categories): ?>
                                <table class="table table-striped table-bordered table-advance table-hover">
                           <thead>
                              <tr>
                                 <th><i class="icon-key"></i> Id</th>
                                 <th><i class="icon-category"></i> عنوان</th>
                                 <th><i class="icon-category"></i> کلمات کلیدی</th>
                                 <th><i class="icon-category"></i> توضیحات</th>
                                 <th class="hidden-phone"></th>
                                 <th class="hidden-phone"></th>
                                 <th><?=$this->Html->link('<i class="icon-plus"></i>',array('action' => 'addCategory'),array('escape' => false))?></th>
                              </tr>
                           </thead>
                           <tbody>

                                   <?php foreach ($categories as $category){
                                       if($category['Category']['del'] == '1'){?>
                           <tr data-href="http://localhost/website/categorys/add">
                                 <td class="highlight">
                                 <?= $category['Category']['id'] ?>
                                 </td>
                                 <td class="hidden-phone"> <?= $category['Category']['title'] ?></td>
                                 <td class="hidden-phone"> <?= $category['Category']['keywords'] ?></td>
                                 <td><?= $category['Category']['describtion'] ?></td>
                                 <td><?= $this->Html->link('<i class="icon-edit"></i>',array(
                                     'action' => 'editCategory',
                                     $category['Category']['id']
                                 ),array('escape' => false)) ?>
                                     </td>
                                 <td>
                                     <?= $this->Html->link('<i class="icon-trash"></i>', array(
                                         'action' => 'deleteCategory',
                                         $category['Category']['id']
                                     ),array('escape' => false)) ?>
                            </tr>
                            <?php
                                       }
                                   }
                            ?>
                        </table>
                                <?php        endif; ?>

                        </div>
                  </div>
               </div>
            </div>
