
<!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="widget">
                        <div class="widget-title">
                           <h4><i class="icon-globe"></i><?= $pageOptions['pageTitle']?></h4>
                                  
                        </div>
                        <div class="widget-body">
                            <?= $this->Form->create('Category',array(
                                'class' => 'form-horizontal'
                            )) ?>

                            <div class="control-group">
                                <div class="control-group">
                                <label class="control-label">عنوان</label>
                                <div class="controls">
                                    <?= $this->Form->input('title', array(
                                        'type' => 'text',
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <div class="control-group">
                            <label class="control-label">کلمات کلیدی</label>
                            <div class="controls">
                                <?= $this->Form->input('keywords', array(
                                    'type' => 'text',
                                    'class' => 'input-xxlarge',
                                    'label' => FALSE,
                                    'div' => FALSE
                                ))?>
                            </div>
                            </div>
                                <label class="control-label">توضیحات</label>
                                <div class="controls">
                                    <?= $this->Form->input('describtion', array(
                                        'type' => 'text',
                                        'row' => '2',
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>       
                            <?= $this->Form->end() ?>

                        </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->  

