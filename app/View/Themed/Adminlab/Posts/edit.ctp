<!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="widget">
                        <div class="widget-title">
                           <h4><i class="icon-globe"></i><?= $pageOptions['pageTitle']?></h4>                                  
                        </div>
                        <div class="widget-body">
                            <?= $this->Form->create('Post',array(
                                'class' => 'form-horizontal'
                            )) ?>
                            <div class="control-group">
                                <label class="control-label">عنوان</label>
                                <div class="controls">
                                    <?= $this->Form->input('title', array(
                                        'type' => 'text',
                                        'value' => $post['Post']['title'],
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">نویسنده</label>
                                <div class="controls">
                                    <?= $this->Form->input('writer', array(
                                        'type' => 'text',
                                        'value' => $post['Post']['writer'],
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">دسته بندی</label>
                                <div class="controls">
                                    <?php
                        $category = array();
                        $j = 0;
                        foreach($post['Category'] as $cat ) {
                            $category[$j] = $cat['id'];
                            $j++;
                        }

                        ?>
                        <input type="hidden" name="data[Category][Category]" value="" id="category_"/>
                        <select name="data[Category][Category][]" id="category" class="span8" multiple="multiple" data-placeholder="انتخاب گروه برای محتوا">
                            <?php
                            foreach($catOptions as $option):
                                ?>
                                <option value="<?php $key = array_search($option ,$catOptions); echo $key; ?>" <?php if(in_array($key, $category)) echo 'selected="selected"'; ?>><?php echo $option; ?></option>
                            <?php endforeach; ?>
                        </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">کلمات کلیدی</label>
                                <div class="controls">
                                    <?= $this->Form->input('keywords', array(
                                        'type' => 'text',
                                        'value' => $post['Post']['keywords'],
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">شرح خبر</label>
                                <div class="controls">
                                    <?= $this->Form->input('summary', array(
                                        'type' => 'text',
                                        'rows' => '2',
                                        'value' => $post['Post']['summary'],
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <!-- BEGIN FORM-->
                                <div class="control-group">
                                    <label class="control-label">متن</label>
                                    <div class="controls">
                                        <?= $this->Form->textarea('text',array(
                                            'class' => 'span12 ckeditor',
                                            'name' => 'editor1',
                                        'value' => $post['Post']['text'],
                                            'rows' => '6',
                                            'id' => 'text',
                                            'readonly' => true,
                                            'dev' => false,
                                            'label' => false
                                        )) ?>
                                    </div>
                                </div>
                                <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>       
                                <?= $this->Form->end() ?>
                            <!-- END FORM-->
                        </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->  
