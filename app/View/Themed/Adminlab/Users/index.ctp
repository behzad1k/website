
<!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                   
                  <div class="widget">
                        <div class="widget-title">
                           <h4><i class="icon-globe"></i><?= $pageOptions['pageTitle']?></h4>
                        </div>
                            <div class="widget-body">
                        <?php if($users): ?>
                                <table class="table table-striped table-bordered table-advance table-hover">
                           <thead>
                              <tr>
                                  <th><i class="icon-key"></i> <?= $this->Paginator->sort('id')?></th>
                                 <th><i class="icon-tag"></i> نام</th>
                                 <th><i class="icon-user"></i> نام کاربری</th>
                                 <th><i class="icon-envelope-alt"></i> ایمیل</th>
                                 <th><i class="icon-phone"></i> تلفن</th>
                                 <th><i class="icon-plus"></i> جنیست</th>
                                 <th><i class="icon-check"></i> وضعیت</th>
                                 <th><i class="icon-group"></i>نوع کاربر</th>
                                 <th class="hidden-phone"></th>
                                 <th class="hidden-phone"></th>
                                 <th class="hidden-phone"></th>
                                 <th class="hidden-phone"></th>
                              </tr>
                           </thead>
                           <tbody>
<ul class="pagination">
<?= $this->Paginator->prev("<<") ?>
<?= $this->Paginator->numbers() ?>
<?= $this->Paginator->next(">>") ?>
</ul>                                   <?php foreach ($users as $user){
                                       if($user['User']['del'] == '1'){?>
                               <?php if($user['User']['id'] == '1'){?>
                               <tr data-href="http://localhost/website/users/add" style="background-color:darkgrey">
                                       <?php }else{ ?>
                                <tr data-href="http://localhost/website/users/add">
                                       <?php }?>
                                   <td class="highlight">
                                 <?= $user['User']['id'] ?>
                                 </td>
                                 <td class="hidden-phone"> <?= $user['User']['name'] ?></td>
                                 <td class="hidden-phone"> <?= $user['User']['username'] ?></td>
                                 <td><?= $user['User']['email'] ?></td>
                                 <td><?= $user['User']['phone'] ?></td>
                                 <td><?php if($user['User']['sex'] == '1'){
                                        echo "مرد";
                                 }
                                 else {
                                    echo "زن";
                                 }
                                 ?></td>
                                 <td><?php if($user['User']['status']== '1'):?>
                                     <p style="background-color:#5ccb10 ;text-align: center;color: white;border-radius: 25px">فعال</p>
                                 
                                 <?php
                                    elseif ($user['User']['status']== '0'):?>
                                     <p style="background-color:#ff0000 ;text-align: center;color: white;border-radius: 25px">غیر فعال</p>
                                     <?php                                 endif;?>
                                 </td>
                                 
                                 <td><?= $user['Group']['title'] ?></td>
                                 
                                 <td><?= $this->Html->link('<i class="icon-user"></i>', array(
                                         'action' => 'profile',
                                         $user['User']['id']
                                     ),array('escape' => false)) ?>
                                 </td>
                                 <td><?= $this->Html->link('<i class="icon-edit"></i>',array(
                                     'action' => 'edit',
                                     $user['User']['id']
                                 ),array('escape' => false)) ?>
                                 </td>
                                 <td><?= $this->Html->link('<i class="icon-key"></i>', array(
                                         'action' => 'changepassword',
                                         $user['User']['id']
                                     ),array('escape' => false)) ?><a style="text-decoration: none" href="#"></a></td>
                                 <td>
                                     <?= $this->Html->link('<i class="icon-trash"></i>', array(
                                         'action' => 'delete',
                                         $user['User']['id']
                                             ),array('escape' => false)) ?></td>
                            </tr>
                              
                            <?php
                                       }
                                   }
                            ?>
                        </table>
                                <?php        endif; ?>

                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->  
