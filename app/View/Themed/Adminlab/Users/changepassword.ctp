
<!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="widget">
                        <div class="widget-title">
                           <h4><i class="icon-globe"></i>Blank Page</h4>
                        </div>
                        <?= $this->Form->create('User') ?>
                        <div class="widget-body">
                        <?php if($superuser ==  $userId): ?>
                            <div class="control-group">
                                <label class="control-label">رمز عبور قبلی</label>
                                <div class="controls">
                                    <?= $this->Form->input('oldpassword', array(
                                        'type' => 'text',
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                            <?php endif; ?>
                            
                            <div class="control-group">
                                <label class="control-label">رمز عبور جدید</label>
                                <div class="controls">
                                    <?= $this->Form->input('password', array(
                                        'type' => 'text',
                                        'class' => 'input-xxlarge',
                                        'label' => FALSE,
                                        'div' => FALSE
                                    ))?>
                                </div>
                            </div>
                                <div class="control-group">
                                    <label class="control-label">تکرار رمز عبور جدید</label>
                                    <div class="controls">
                                        <?= $this->Form->input('repassword', array(
                                            'type' => 'text',
                                            'class' => 'input-xxlarge',
                                            'label' => FALSE,
                                            'div' => FALSE
                                        ))?>
                                    </div>
                                </div>
                            <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>    
                        </div>
                      <?= $this->Form->end() ?>
                  </div>
               </div>
            </div>

            <!-- END PAGE CONTENT-->  
