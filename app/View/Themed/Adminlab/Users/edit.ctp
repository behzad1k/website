
<!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="widget">
                        <div class="widget-title">
                           <h4><i class="icon-globe"></i>ویرایش کاربر</h4>
                                  
                        </div>
                        <div class="widget-body">
                            <?= $this->Form->create('User', array('class' => 'form-horizontal')) ?>
                                <div class="control-group">
                                    <label class="control-label">نام</label>
                                    <div class="controls">
                                        <?= $this->Form->input('name', array(
                                            'value' => $user['User']['name'],
                                            'type' => 'text',
                                            'class' => 'input-xxlarge',
                                            'label' => FALSE,
                                            'div' => FALSE
                                        ))?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">نام کاربری</label>
                                    <div class="controls" >
                                        <?= $this->Form->input('username', array(
                                            'type' => 'text',
                                            'value' => $user['User']['username'],
                                            'class' => 'input-xxlarge',
                                            'label' => FALSE,
                                            'div' => FALSE
                                        ))?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">ایمیل</label>
                                    <div class="controls">
                                        <?= $this->Form->input('email', array(
                                            'type' => 'text',
                                            'value' => $user['User']['email'],
                                            'class' => 'input-xxlarge',
                                            'label' => FALSE,
                                            'div' => FALSE
                                        ))?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">شماره تماس</label>
                                    <div class="controls">
                                        <?= $this->Form->input('phone', array(
                                            'type' => 'text',
                                            'value' => $user['User']['phone'],
                                            'class' => 'input-xxlarge',
                                            'label' => FALSE,
                                            'div' => FALSE
                                        ))?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">تاریخ تولد</label>
                                    <div class="controls">
                                        <div class="input-append date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                        <?= $this->Form->input('birthdate', array(
                                            'type' => 'text',
                                            'value' => $user['User']['birthdate'],
                                            'class' => ' m-ctrl-medium date-picker',
                                            'size' => '16',
                                            'label' => FALSE,
                                            'div' => FALSE
                                        ))?>
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                            </div>
                                    </div>
                            </div>
                                <div class="control-group">
                                    <label class="control-label">جنسیت</label>
                                    <div class="controls">
                                        <?= $this->Form->select('sex', array(
                                            'زن','مرد'
                                        ),array(
                                            'tabindex' => '-1',
                                            'value' => $user['User']['sex'],
                                            'class' => 'span6 chosen chzn-done',
                                            'id' => 'selP8V',
                                            'display' => 'none',
                                            'label' => FALSE,
                                            'div' => FALSE
                                        ))?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">فعال</label>
                                    <div class="controls">
                                        <div class="basic-toggle-button">
                                        <?= $this->Form->input('status', array(
                                            'type' => 'checkbox',
                                            'class' => 'toggle',
                                            'checked' => 'checked',
                                            'label' => FALSE,
                                            'div' => FALSE
                                        ))?>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">آدرس</label>
                                    <div class="controls">
                                        <?= $this->Form->input('address', array(
                                            'type' => 'textarea',
                                            'value' => $user['User']['address'],
                                            'class' => 'input-xxlarge',
                                            'rows' => '3',
                                            'label' => FALSE,
                                            'div' => FALSE
                                        ))?>
                                    </div>
                                </div>
                            <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>       
                            <?= $this->Form->end(); ?>
                        </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->  