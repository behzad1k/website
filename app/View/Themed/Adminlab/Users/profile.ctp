

<!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="widget">
                        <div class="widget-title">
                           <h4><i class="icon-globe"></i>Blank Page</h4>
                                  
                        </div>
                        <div class="widget-body">
                            <div class="widget-body">
                            <div class="span3">
                                <div class="text-center profile-pic">
                                    <img src="img/profile-pic.jpg" alt="">
                                </div>
                                <ul class="nav nav-tabs nav-stacked">
                                    <li><a href="javascript:void(0)"><i class="icon-coffee"></i> Portfolio</a></li>
                                    <li><a href="javascript:void(0)"><i class="icon-paper-clip"></i> Projects</a></li>
                                    <li><a href="javascript:void(0)"><i class="icon-picture"></i> Gallery</a></li>
                                </ul>
                                <ul class="nav nav-tabs nav-stacked">
                                    <li><a href="javascript:void(0)"><i class="icon-facebook"></i> Facebook</a></li>
                                    <li><a href="javascript:void(0)"><i class="icon-twitter"></i> Twitter</a></li>
                                    <li><a href="javascript:void(0)"><i class="icon-linkedin"></i> LinkedIn</a></li>
                                    <li><a href="javascript:void(0)"><i class="icon-pinterest"></i> Pinterest</a></li>
                                    <li><a href="javascript:void(0)"><i class="icon-github"></i> Github</a></li>
                                </ul>
                            </div>
                            <div class="span6">
                                <h4><?= $user['User']['name']?> <br><small>Frontend Developer</small></h4>
                                <table class="table table-borderless">
                                    <tbody>
                                    
                                    <tr>
                                        <td class="span2">Username :</td>
                                        <td>
                                            <?= $user['User']['username']?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2">Email :</td>
                                        <td>
                                            <?= $user['User']['email']?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2">Birthday :</td>
                                        <td>
                                            13 july 1983
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2">Gender :</td>
                                        <td>
                                            <?php if($user['User']['sex'] == '1'){
                                        echo "مرد";
                                 }
                                 else {
                                    echo "زن";
                                 }?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2"> Phone :</td>
                                        <td>
                                            <?= $user['User']['phone']?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span2"> Group :</td>
                                        <td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <h4>About</h4>

                                <p class="push">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor. Vestibulum ullamcorper, odio sed rhoncus imperdiet, enim elit sollicitudin orci, eget dictum leo mi nec lectus. Nam commodo turpis id lectus scelerisque vulputate. Integer sed dolor erat. Fusce erat ipsum, varius vel euismod sed, tristique et lectus? Etiam egestas fringilla enim, id convallis lectus laoreet at. Fusce purus nisi, gravida sed consectetur ut, interdum quis nisi. Quisque egestas nisl id lectus facilisis scelerisque? Proin rhoncus dui at ligula vestibulum ut facilisis ante sodales! Suspendisse potenti. Aliquam tincidunt sollicitudin sem nec ultrices. Sed at mi velit. Ut egestas tempor est, in cursus enim venenatis eget! Nulla quis ligula ipsum. Donec vitae ultrices dolor?</p>
                                <h4>Skills</h4>

                                <table class="table table-borderless">
                                    <tbody>
                                    <tr>
                                        <td class="span1"><span class="label label-inverse">HTML</span></td>
                                        <td>
                                            <div class="progress progress-success progress-striped">
                                                <div style="width: 90%" class="bar"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span1"><span class="label label-inverse">CSS</span></td>
                                        <td>
                                            <div class="progress progress-warning progress-striped">
                                                <div style="width: 85%" class="bar"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span1"><span class="label label-inverse">Javascript</span></td>
                                        <td>
                                            <div class="progress progress-success progress-striped">
                                                <div style="width: 60%" class="bar"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span1"><span class="label label-inverse">PHP</span></td>
                                        <td>
                                            <div class="progress progress-success progress-striped">
                                                <div style="width: 40%" class="bar"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span1"><span class="label label-inverse">Photoshop</span></td>
                                        <td>
                                            <div class="progress progress-warning progress-striped">
                                                <div style="width: 80%" class="bar"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span1"><span class="label label-inverse">Node.js</span></td>
                                        <td>
                                            <div class="progress progress-danger progress-striped">
                                                <div style="width: 45%" class="bar"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="span1"><span class="label label-inverse">Java</span></td>
                                        <td>
                                            <div class="progress progress-danger progress-striped">
                                                <div style="width: 10%" class="bar"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <h4>Address</h4>
                                <div class="well">
                                    <address>
                                        <strong><?= $user['User']['address']?></strong><br>
                                        
                                        <abbr title="Phone">P:</abbr> (123) 456-7891
                                    </address>
                                    <address>
                                        <strong>Full Name</strong><br>
                                        <a href="mailto:#">first.last@gmail.com</a>
                                    </address>
                                </div>
                            </div>
                            <div class="span3">
                                                                <h4>Current Status</h4>
                                <div class="alert alert-success"><i class="icon-ok-sign"></i> Working in ABC Company</div>
                                
                            </div>
                            <div class="space5"></div>
                        </div>
                        </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->  