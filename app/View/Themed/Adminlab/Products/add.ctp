<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
	<div class="span12">
		<div class="widget">
			<div class="widget-title">
				<h4><i class="icon-globe"></i><?= $pageOptions['pageTitle']?></h4>
			</div>
			<div class="widget-body">
				<?= $this->Form->create('Product',array(
						'class' => 'form-horizontal',
						'type' => 'file'
				)) ?>
				<div class="control-group">
					<label class="control-label">عنوان</label>
					<div class="controls">
						<?= $this->Form->input('title', array(
								'type' => 'text',
								'class' => 'input-xxlarge',
								'label' => FALSE,
								'div' => FALSE
						))?>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">تصویر</label>
					<div class="controls">
						<?= $this->Form->input('pic', array(
								'type' => 'file',
								'class' => 'input-xxlarge',
								'label' => FALSE,
								'div' => FALSE
						))?>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label">شرح</label>
					<div class="controls">
						<?= $this->Form->input('info', array(
								'type' => 'text',
								'class' => 'input-xxlarge',
								'label' => FALSE,
								'div' => FALSE
						))?>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">قیمت</label>
					<div class="controls">
						<?= $this->Form->input('price', array(
								'type' => 'number',
								'class' => 'input-xxlarge',
								'label' => FALSE,
								'div' => FALSE
						))?>
					</div>
				</div>
				<button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
