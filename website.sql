-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 24, 2020 at 08:07 PM
-- Server version: 5.7.31-0ubuntu0.18.04.1
-- PHP Version: 7.2.33-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `website`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `keywords` varchar(11) NOT NULL,
  `describtion` varchar(100) DEFAULT NULL,
  `del` varchar(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `keywords`, `describtion`, `del`, `created`, `modified`) VALUES
(1, 'khabar', 'mohem', 'khabar mohem nist', '1', '2020-07-30 07:39:18', '2020-07-30 08:11:12'),
(3, 'hadese', 'hadeseh', 'rokhdad', '0', '2020-07-30 08:08:04', '2020-07-30 08:16:04');

-- --------------------------------------------------------

--
-- Table structure for table `categories_posts`
--

CREATE TABLE `categories_posts` (
  `category_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories_posts`
--

INSERT INTO `categories_posts` (`category_id`, `post_id`) VALUES
(1, 5),
(3, 5),
(1, 6),
(1, 7),
(3, 7);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `title` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `title`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(4) NOT NULL,
  `created` datetime NOT NULL,
  `ip` varchar(35) NOT NULL,
  `op` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user_id`, `action`, `created`, `ip`, `op`) VALUES
(1, 1, '0101', '2020-07-28 12:18:20', '127.0.0.1', '2'),
(2, 2, '0100', '2020-07-29 06:03:18', '127.0.0.1', '1'),
(3, 1, '0100', '2020-07-29 06:05:13', '127.0.0.1', '1'),
(4, 1, '0100', '2020-07-29 07:16:42', '127.0.0.1', '1'),
(5, 1, '0100', '2020-07-29 07:16:54', '127.0.0.1', '1'),
(6, 1, '0100', '2020-07-29 07:17:03', '127.0.0.1', '1'),
(7, 1, '0100', '2020-07-29 07:17:43', '127.0.0.1', '1'),
(8, 1, '0100', '2020-07-29 07:17:52', '127.0.0.1', '1'),
(9, 1, '0100', '2020-07-29 07:18:23', '127.0.0.1', '1'),
(10, 2, '0100', '2020-07-29 07:19:17', '127.0.0.1', '1'),
(11, 2, '0100', '2020-07-29 07:24:42', '127.0.0.1', '1'),
(12, 2, '0100', '2020-07-29 07:49:10', '127.0.0.1', '1'),
(13, 2, '0100', '2020-07-29 13:02:10', '127.0.0.1', '1'),
(14, 2, '0100', '2020-07-30 04:39:36', '127.0.0.1', '1'),
(15, 1, '0103', '2020-07-30 06:29:28', '127.0.0.1', '1'),
(16, 1, '0202', '2020-07-30 06:54:21', '127.0.0.1', '1'),
(17, 1, '0301', '2020-07-30 07:39:18', '127.0.0.1', '1'),
(18, 1, '0301', '2020-07-30 07:40:47', '127.0.0.1', '2'),
(19, 1, '0302', '2020-07-30 07:54:19', '127.0.0.1', '1'),
(20, 1, '0302', '2020-07-30 07:54:23', '127.0.0.1', '1'),
(21, 1, '0302', '2020-07-30 07:54:25', '127.0.0.1', '1'),
(22, 1, '0302', '2020-07-30 07:59:08', '127.0.0.1', '1'),
(23, 1, '0302', '2020-07-30 08:01:38', '127.0.0.1', '1'),
(24, 1, '0302', '2020-07-30 08:03:10', '127.0.0.1', '1'),
(25, 1, '0302', '2020-07-30 08:03:24', '127.0.0.1', '1'),
(26, 1, '0302', '2020-07-30 08:07:08', '127.0.0.1', '1'),
(27, 1, '0301', '2020-07-30 08:08:04', '127.0.0.1', '3'),
(28, 1, '0302', '2020-07-30 08:08:24', '127.0.0.1', '3'),
(29, 1, '0302', '2020-07-30 08:09:52', '127.0.0.1', '1'),
(30, 1, '0302', '2020-07-30 08:11:12', '127.0.0.1', '1'),
(31, 1, '0303', '2020-07-30 08:16:04', '127.0.0.1', '3'),
(32, 1, '0201', '2020-07-30 08:32:02', '127.0.0.1', '2'),
(33, 1, '0201', '2020-07-30 08:33:11', '127.0.0.1', '3'),
(34, 1, '0201', '2020-07-30 08:39:35', '127.0.0.1', '4'),
(35, 2, '0100', '2020-07-30 08:41:00', '127.0.0.1', '1'),
(36, 1, '0201', '2020-07-30 08:48:45', '127.0.0.1', '5'),
(37, 1, '0103', '2020-07-30 08:49:34', '127.0.0.1', '5'),
(38, 1, '0201', '2020-07-30 08:50:25', '127.0.0.1', '6'),
(39, 2, '0100', '2020-07-30 08:59:57', '127.0.0.1', '1'),
(40, 2, '0100', '2020-08-01 10:06:19', '127.0.0.1', '1'),
(41, 2, '0100', '2020-08-01 06:03:23', '127.0.0.1', '1'),
(42, 1, '0102', '2020-08-01 08:12:41', '127.0.0.1', '2'),
(43, 2, '0100', '2020-08-01 10:05:48', '127.0.0.1', '1'),
(44, 1, '0102', '2020-08-01 10:25:29', '127.0.0.1', '2'),
(45, 1, '0102', '2020-08-01 10:25:36', '127.0.0.1', '2'),
(46, 1, '0102', '2020-08-01 10:34:17', '127.0.0.1', '1'),
(47, 1, '0102', '2020-08-01 10:34:24', '127.0.0.1', '2'),
(48, 1, '0102', '2020-08-01 10:34:32', '127.0.0.1', '2'),
(49, 1, '0102', '2020-08-01 10:34:37', '127.0.0.1', '1'),
(50, 1, '0102', '2020-08-01 10:34:42', '127.0.0.1', '1'),
(51, 1, '0102', '2020-08-01 10:34:48', '127.0.0.1', '1'),
(52, 1, '0102', '2020-08-01 10:34:54', '127.0.0.1', '1'),
(53, 1, '0102', '2020-08-01 10:34:59', '127.0.0.1', '1'),
(54, 1, '0102', '2020-08-01 10:35:05', '127.0.0.1', '1'),
(55, 2, '0100', '2020-08-01 10:51:53', '127.0.0.1', '1'),
(56, 1, '0102', '2020-08-01 10:52:45', '127.0.0.1', '2'),
(57, 1, '0102', '2020-08-01 10:52:49', '127.0.0.1', '2'),
(58, 1, '0102', '2020-08-01 10:52:59', '127.0.0.1', '2'),
(59, 1, '0102', '2020-08-01 10:53:08', '127.0.0.1', '2'),
(60, 1, '0102', '2020-08-01 10:54:35', '127.0.0.1', '1'),
(61, 2, '0100', '2020-08-02 05:58:13', '127.0.0.1', '1'),
(62, 1, '0202', '2020-08-02 06:18:31', '127.0.0.1', '6'),
(63, 2, '0100', '2020-08-02 06:37:46', '127.0.0.1', '1'),
(64, 1, '0201', '2020-08-02 07:37:52', '127.0.0.1', '7'),
(65, 1, '0201', '2020-08-02 07:38:12', '127.0.0.1', '8'),
(66, 1, '0201', '2020-08-02 07:40:01', '127.0.0.1', '9'),
(67, 1, '0201', '2020-08-02 07:41:11', '127.0.0.1', '10'),
(68, 1, '0201', '2020-08-02 07:42:50', '127.0.0.1', '11'),
(69, 1, '0201', '2020-08-02 07:43:43', '127.0.0.1', '12'),
(70, 1, '0201', '2020-08-02 07:45:54', '127.0.0.1', '13'),
(71, 1, '0201', '2020-08-02 07:46:14', '127.0.0.1', '14'),
(72, 1, '0201', '2020-08-02 07:47:51', '127.0.0.1', '15'),
(73, 1, '0201', '2020-08-02 07:49:19', '127.0.0.1', '16'),
(74, 1, '0201', '2020-08-02 07:50:04', '127.0.0.1', '17'),
(75, 2, '0100', '2020-08-03 05:45:11', '127.0.0.1', '1'),
(76, 2, '0100', '2020-08-03 06:41:40', '127.0.0.1', '1'),
(77, 2, '0100', '2020-08-31 18:30:09', '127.0.0.1', '3'),
(78, 2, '0100', '2020-08-31 18:34:51', '127.0.0.1', '3'),
(79, 2, '0100', '2020-11-10 09:23:51', '127.0.0.1', '3'),
(80, 2, '0100', '2020-11-11 10:51:38', '127.0.0.1', '3'),
(81, 2, '0100', '2020-11-17 14:34:01', '127.0.0.1', '3'),
(82, 2, '0100', '2020-11-17 14:35:33', '127.0.0.1', '3'),
(83, 2, '0100', '2020-11-17 14:36:21', '127.0.0.1', '3');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `text` text,
  `keywords` varchar(200) NOT NULL,
  `summary` varchar(400) NOT NULL,
  `writer` varchar(100) NOT NULL,
  `del` varchar(1) NOT NULL DEFAULT '1',
  `picture` varchar(105) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `text`, `keywords`, `summary`, `writer`, `del`, `picture`, `created`, `modified`) VALUES
(1, 'cake', 'cakephp is usefull', 'cakephp', 'cakephp usefull', 'behzad', '1', NULL, '2020-07-30 06:19:43', '2020-07-30 06:54:21'),
(2, 'post', NULL, 'khabarha', 'khabar', 'admin', '1', NULL, '2020-07-30 08:32:02', '2020-07-30 08:32:02');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `info` varchar(200) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `sex` varchar(1) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `del` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `status`, `email`, `phone`, `address`, `birthdate`, `sex`, `group_id`, `created`, `modified`, `del`) VALUES
(2, 'guest', 'guest', '0f473f720047d161af22f65fce29a834b6deace0', '1', 'bhzd1k@gmail.com', '09165117471', 'semnan', NULL, '1', 2, '2020-07-28 12:18:20', '2020-08-01 10:53:08', '1'),
(3, 'admin', 'admin', '7cfe3d019c0d6155b663749c0a34def33fbf071b', '1', 'bhzd1k@gmail.com', '09379455353', 'semnan', NULL, '1', 1, '2020-07-28 11:18:58', '2020-08-01 10:54:35', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
